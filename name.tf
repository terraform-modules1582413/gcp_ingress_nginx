module "name" {
  source = "git::https://gitlab.com/terraform-modules1582413/name.git"

  prefix      = var.project_prefix
  environment = var.project_environment
  name        = var.ingress_class_name
  resource    = "${var.ingress_class_name}-ingress"
}
