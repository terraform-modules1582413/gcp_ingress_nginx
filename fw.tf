resource "google_compute_firewall" "google_to_hc" {
  project = var.gcp_network_project_id
  network = var.gcp_network_name

  name = "${module.name.short}-fw-allow-google-to-hc-${module.name.appendix}"

  allow {
    protocol = "tcp"
    ports    = [var.ingress_hc_port]
  }

  source_ranges = var.gcp_hc_ips_cidrs
  target_tags   = var.ingress_node_pool_network_tags

  priority = 15
}

resource "google_compute_firewall" "default_fw_rules" {
  for_each = { for rule in var.ingress_default_fw_rules : rule.name => rule }

  project = var.gcp_network_project_id
  network = var.gcp_network_name

  name = "${module.name.short}-fw-allow-${each.value.name}-${module.name.appendix}"

  allow {
    protocol = "tcp"
    ports    = flatten([["443"], each.value.http_enabled ? ["80"] : []])
  }

  source_ranges = each.value.ips_cidrs
  target_tags   = var.ingress_node_pool_network_tags
  priority      = 20

  dynamic "log_config" {
    for_each = each.value.logging_enabled ? [1] : []
    content {
      metadata = "INCLUDE_ALL_METADATA"
    }
  }
}

resource "google_compute_firewall" "global_fw_rules" {
  for_each = { for rule in var.ingress_global_fw_rules : rule.name => rule }

  project = var.gcp_network_project_id
  network = var.gcp_network_name

  name = "${module.name.short}-fw-allow-${each.value.name}-${module.name.appendix}"

  allow {
    protocol = "tcp"
    ports    = flatten([["443"], each.value.http_enabled ? ["80"] : []])
  }

  source_ranges = each.value.ips_cidrs
  target_tags   = var.ingress_node_pool_network_tags
  priority      = 25

  dynamic "log_config" {
    for_each = each.value.logging_enabled ? [1] : []
    content {
      metadata = "INCLUDE_ALL_METADATA"
    }
  }
}
