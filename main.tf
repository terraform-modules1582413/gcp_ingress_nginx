variable "tolerations_mapping" {
  default = {
    "NO_SCHEDULE"        = "NoSchedule",
    "PREFER_NO_SCHEDULE" = "PreferNoSchedule",
    "NO_EXECUTE"         = "NoExecute"
  }
}

locals {
  tolerations = [
    for toleration in var.ingress_node_pool_taints : {
      key      = toleration.key
      value    = toleration.value
      effect   = lookup(var.tolerations_mapping, toleration.effect)
      operator = "Equal"
    }
  ]
}

resource "google_compute_address" "public_ip" {
  project      = var.gcp_project_id
  name         = "${module.name.short}-public-ip-${module.name.appendix}"
  address_type = "EXTERNAL"
  region       = var.gcp_region
}

resource "helm_release" "ingress" {
  name             = "ingress-${var.ingress_class_name}"
  chart            = "ingress-nginx"
  repository       = "https://kubernetes.github.io/ingress-nginx"
  create_namespace = var.ingress_create_namespace_enabled
  namespace        = var.ingress_namespace
  version          = "4.6.0"

  values = [
    templatefile("${path.module}/ingress-values.yaml", {
      public_ip      = google_compute_address.public_ip.address
      hc_port        = var.ingress_hc_port
      class_name     = var.ingress_class_name
      tolerations    = local.tolerations
      node_selectors = var.ingress_node_pool_selectors
      autoscaling    = var.ingress_autoscaling
    })
  ]

  depends_on = [google_compute_firewall.google_to_hc, google_compute_address.public_ip]
}

resource "time_sleep" "wait_45_seconds" {
  depends_on = [helm_release.ingress]

  destroy_duration = "45s"
}
