config:
      log-format-escape-json: "true"
      log-format-upstream: '{"timestamp": "$time_iso8601", "requestID": "$req_id", "proxyUpstreamName":
      "$proxy_upstream_name", "proxyAlternativeUpstreamName": "$proxy_alternative_upstream_name","upstreamStatus":
      "$upstream_status", "upstreamAddr": "$upstream_addr","httpRequest":{"requestMethod":
      "$request_method", "requestUrl": "$host$request_uri", "status": $status,"requestSize":
      "$request_length", "responseSize": "$upstream_response_length", "userAgent": "$http_user_agent",
      "remoteIp": "$remote_addr", "referer": "$http_referer", "latency": "$upstream_response_time s",
      "protocol":"$server_protocol"}}'
      ssl-protocols: "TLSv1.3"
      ssl-session-cache: "true"
      ssl-session-cache-size: "shared:SSL:10m"
      ssl-session-timeout: "1d"
      ssl-session-tickets: "false"

#      generate-request-id: "true"

#      proxy-body-size: "2m"
#      keep-alive: "60"
#      keep-alive-requests: "100"

#      enable-brotli: false
#      brotli-level: 4
#      brotli-types: "application/json"

#      use-http2: false

#      use-gzip: false
#      gzip-level: 1
#      gzip-types: "application/json"

#      upstream-keepalive-connections: 320
#      upstream-keepalive-time: "1h"
#      upstream-keepalive-timeout: 60
#      upstream-keepalive-requests: 10000

#      proxy-connect-timeout: "60"
#      proxy-read-timeout: "60"
#      proxy-send-timeout: "60"
#      proxy-next-upstream:
#      proxy-next-upstream-timeout:
#      proxy-next-upstream-tries:

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_google"></a> [google](#requirement\_google) | ~> 4.59.0 |
| <a name="requirement_helm"></a> [helm](#requirement\_helm) | ~> 2.9.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | ~> 4.59.0 |
| <a name="provider_helm"></a> [helm](#provider\_helm) | ~> 2.9.0 |
| <a name="provider_time"></a> [time](#provider\_time) | n/a |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_name"></a> [name](#module\_name) | git::https://gitlab.com/terraform-modules1582413/name.git | n/a |

## Resources

| Name | Type |
|------|------|
| [google_compute_address.public_ip](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_address) | resource |
| [google_compute_firewall.default_fw_rules](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_firewall) | resource |
| [google_compute_firewall.global_fw_rules](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_firewall) | resource |
| [google_compute_firewall.google_to_hc](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_firewall) | resource |
| [helm_release.ingress](https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release) | resource |
| [time_sleep.wait_45_seconds](https://registry.terraform.io/providers/hashicorp/time/latest/docs/resources/sleep) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_gcp_hc_ips_cidrs"></a> [gcp\_hc\_ips\_cidrs](#input\_gcp\_hc\_ips\_cidrs) | n/a | `list(string)` | <pre>[<br>  "130.211.0.0/22",<br>  "35.191.0.0/16",<br>  "209.85.152.0/22",<br>  "209.85.204.0/22"<br>]</pre> | no |
| <a name="input_gcp_network_name"></a> [gcp\_network\_name](#input\_gcp\_network\_name) | n/a | `string` | n/a | yes |
| <a name="input_gcp_network_project_id"></a> [gcp\_network\_project\_id](#input\_gcp\_network\_project\_id) | n/a | `string` | n/a | yes |
| <a name="input_gcp_project_id"></a> [gcp\_project\_id](#input\_gcp\_project\_id) | n/a | `string` | n/a | yes |
| <a name="input_gcp_region"></a> [gcp\_region](#input\_gcp\_region) | n/a | `string` | n/a | yes |
| <a name="input_ingress_autoscaling"></a> [ingress\_autoscaling](#input\_ingress\_autoscaling) | n/a | <pre>object({<br>    enabled      = bool<br>    min_replicas = number<br>    max_replicas = number<br>  })</pre> | <pre>{<br>  "enabled": false,<br>  "max_replicas": 1,<br>  "min_replicas": 2<br>}</pre> | no |
| <a name="input_ingress_class_name"></a> [ingress\_class\_name](#input\_ingress\_class\_name) | n/a | `string` | `"nginx"` | no |
| <a name="input_ingress_create_namespace_enabled"></a> [ingress\_create\_namespace\_enabled](#input\_ingress\_create\_namespace\_enabled) | n/a | `bool` | `false` | no |
| <a name="input_ingress_default_fw_rules"></a> [ingress\_default\_fw\_rules](#input\_ingress\_default\_fw\_rules) | n/a | <pre>list(object({<br>    name            = string<br>    ips_cidrs       = list(string)<br>    logging_enabled = bool<br>    http_enabled    = bool<br>  }))</pre> | `[]` | no |
| <a name="input_ingress_global_fw_rules"></a> [ingress\_global\_fw\_rules](#input\_ingress\_global\_fw\_rules) | n/a | <pre>list(object({<br>    name            = string<br>    ips_cidrs       = list(string)<br>    logging_enabled = bool<br>    http_enabled    = bool<br>  }))</pre> | `[]` | no |
| <a name="input_ingress_hc_port"></a> [ingress\_hc\_port](#input\_ingress\_hc\_port) | n/a | `string` | `"30001"` | no |
| <a name="input_ingress_namespace"></a> [ingress\_namespace](#input\_ingress\_namespace) | n/a | `string` | n/a | yes |
| <a name="input_ingress_node_pool_network_tags"></a> [ingress\_node\_pool\_network\_tags](#input\_ingress\_node\_pool\_network\_tags) | n/a | `list(string)` | n/a | yes |
| <a name="input_ingress_node_pool_selectors"></a> [ingress\_node\_pool\_selectors](#input\_ingress\_node\_pool\_selectors) | n/a | `map(any)` | `{}` | no |
| <a name="input_ingress_node_pool_taints"></a> [ingress\_node\_pool\_taints](#input\_ingress\_node\_pool\_taints) | n/a | <pre>list(object({<br>    key    = string<br>    value  = string<br>    effect = string<br>  }))</pre> | `[]` | no |
| <a name="input_project_environment"></a> [project\_environment](#input\_project\_environment) | n/a | `string` | n/a | yes |
| <a name="input_project_prefix"></a> [project\_prefix](#input\_project\_prefix) | n/a | `string` | n/a | yes |
| <a name="input_tolerations_mapping"></a> [tolerations\_mapping](#input\_tolerations\_mapping) | n/a | `map` | <pre>{<br>  "NO_EXECUTE": "NoExecute",<br>  "NO_SCHEDULE": "NoSchedule",<br>  "PREFER_NO_SCHEDULE": "PreferNoSchedule"<br>}</pre> | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_ingress_class"></a> [ingress\_class](#output\_ingress\_class) | n/a |
| <a name="output_public_ip"></a> [public\_ip](#output\_public\_ip) | n/a |
<!-- END_TF_DOCS -->