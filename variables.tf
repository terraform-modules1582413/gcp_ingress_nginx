variable "project_prefix" {
  type = string
}
variable "project_environment" {
  type = string
}
variable "gcp_region" {
  type = string
}
variable "gcp_project_id" {
  type = string
}
variable "gcp_network_name" {
  type = string
}
variable "gcp_network_project_id" {
  type = string
}
variable "gcp_hc_ips_cidrs" {
  type    = list(string)
  default = ["130.211.0.0/22", "35.191.0.0/16", "209.85.152.0/22", "209.85.204.0/22"]
}
variable "ingress_node_pool_network_tags" {
  type = list(string)
}
variable "ingress_node_pool_selectors" {
  type    = map(any)
  default = {}
}
variable "ingress_node_pool_taints" {
  type = list(object({
    key    = string
    value  = string
    effect = string
  }))
  default = []
}
variable "ingress_create_namespace_enabled" {
  type    = bool
  default = false
}
variable "ingress_namespace" {
  type = string
}
variable "ingress_class_name" {
  type    = string
  default = "nginx"
}
variable "ingress_hc_port" {
  type    = number
  default = 30000
  validation {
    condition     = var.ingress_hc_port >= 30000 && var.ingress_hc_port <= 32767
    error_message = "The range of valid ports is 30000-32767"
  }
}
variable "ingress_default_fw_rules" {
  type = list(object({
    name            = string
    ips_cidrs       = list(string)
    logging_enabled = bool
    http_enabled    = bool
  }))
  default = []
}
variable "ingress_global_fw_rules" {
  type = list(object({
    name            = string
    ips_cidrs       = list(string)
    logging_enabled = bool
    http_enabled    = bool
  }))
  default = []
}
variable "ingress_autoscaling" {
  type = object({
    enabled      = bool
    min_replicas = number
    max_replicas = number
  })
  default = {
    enabled      = false
    max_replicas = 1
    min_replicas = 2
  }
}
