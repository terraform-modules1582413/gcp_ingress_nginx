output "public_ip" {
  value = google_compute_address.public_ip.address
}

output "ingress_class" {
  value = var.ingress_class_name
}